// This ensures that jquery is imported and assigned to globals prior to any imports that occur in main.js
import jquery from 'jquery';
window.jQuery = jquery;
window.$ = jquery;
