import jqueryglobals from './jqueryglobals.js'
import bootstrap from 'bootstrap'
import firebase from 'firebase'
import moment from 'moment'
import Highcharts from 'highcharts'
import config from '../config.json'

let dbConfig = config.dbConfig
let speeds = []
let times = []
let latency = []

firebase.initializeApp(dbConfig)
const database = firebase.database()

// Get a reference to the database location, order them and limit to a days worth of results
let ref = database.ref('tests/').orderByKey().limitToLast(156)

// on data create speeds and date arrays
ref.on('value', (data) => {

    // clean up the arrays
    speeds = []
    times = []
    latency = []

    data.forEach((el) => {
        speeds.push(el.val().speeds.download)
        latency.push(el.val().server.ping)
        times.push(moment(Number(el.key)).format('M/D/YY HH:mm'))
    })

    var speedsChart = Highcharts.chart('speedCharter', {
        chart: {
            type: 'line',
            zoomType: "xy"
        },
        title: {
            text: 'Download Speeds'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
        },
        xAxis: {
            categories: times
        },
        yAxis: {
            title: {
                text: 'MBPS'
            }
        },
        series: [{
            name: 'MBPS',
            data: speeds
        }]
    })

    var latencyChart = Highcharts.chart('latencyCharter', {
        chart: {
            type: 'line',
            zoomType: "xy"
        },
        title: {
            text: 'Latency'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
        },
        xAxis: {
            categories: times
        },
        yAxis: {
            title: {
                text: 'ms'
            }
        },
        series: [{
            name: 'ms',
            data: latency
        }]

    })
})
